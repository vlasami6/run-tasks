use std::collections::HashMap;
use std::{alloc, mem, ptr};

use crate::bytecode::*;
use crate::value::*;

type Result<T> = std::result::Result<T, &'static str>;

pub struct Constants {
    constants: Vec<BcObject>,
    labels: HashMap<ConstantIndex, usize>,
}

impl Constants {
    pub fn new(constants: Vec<BcObject>, labels: HashMap<ConstantIndex, usize>) -> Self {
        Self { constants, labels }
    }

    pub fn get(&self, index: ConstantIndex) -> Result<&BcObject> {
        self.constants
            .get(index.0 as usize)
            .ok_or("invalid constant index")
    }

    pub fn label_offset(&self, label: ConstantIndex) -> Result<usize> {
        self.labels.get(&label).copied().ok_or("unknown label")
    }
}

pub struct FrameStack {
    locals: Vec<Value>,
    base: usize,
}

impl FrameStack {
    pub fn new() -> Self {
        Self {
            locals: Vec::new(),
            base: 0,
        }
    }

    pub fn push(ctx: &mut VMContext, count: LocalIndex) {
        // GCNOTE: unrooted temporary gets rooted immediately
        let saved_base = ctx.create_int(ctx.frame_stack.base as i32);
        ctx.frame_stack.locals.push(saved_base);
        //self.locals.push(Value::Integer(self.base as i32));
        ctx.frame_stack.base = ctx.frame_stack.locals.len();
        // + 1 for old base pointer
        //self.locals.resize(self.base + count.0 as usize, Value::Null);
        ctx.frame_stack
            .locals
            .resize(ctx.frame_stack.base + count.0 as usize, Value::null());
    }

    pub fn pop(&mut self, count: LocalIndex) -> Result<()> {
        self.locals.truncate(self.locals.len() - count.0 as usize);
        self.base = self.locals.pop().ok_or("return at top level")?.as_integer() as usize;
        Ok(())
    }

    pub fn get_local(&self, index: LocalIndex) -> Result<Value> {
        Ok(*self
            .locals
            .get(self.base + index.0 as usize)
            .ok_or("invalid local index")?)
    }

    pub fn set_local(&mut self, index: LocalIndex, value: Value) -> Result<()> {
        let value_ref = self
            .locals
            .get_mut(self.base + index.0 as usize)
            .ok_or("invalid local index")?;
        *value_ref = value;
        Ok(())
    }
}

impl<'a> IntoIterator for &'a mut FrameStack {
    type Item = &'a mut Value;
    type IntoIter = std::slice::IterMut<'a, Value>;
    fn into_iter(self) -> Self::IntoIter {
        self.locals.iter_mut()
    }
}

pub struct OperandStack {
    stack: Vec<Value>,
}

impl OperandStack {
    pub fn new() -> Self {
        Self { stack: Vec::new() }
    }

    pub fn push(&mut self, value: Value) {
        //eprintln!("push: {:?} {:?}", self.stack, value);
        self.stack.push(value);
    }

    pub fn peek(&self) -> Result<Value> {
        //eprintln!("peek: {:?}", self.stack);
        Ok(*self.stack.last().ok_or("invalid stack peek")?)
    }

    pub fn peek_below(&self, from_top: u8) -> Result<Value> {
        //eprintln!("peek below {}: {:?}", from_top, self.stack);
        Ok(*self
            .stack
            .get(self.stack.len() - 1 - from_top as usize)
            .ok_or("invalid stack peek below")?)
    }

    pub fn replace_below(&mut self, from_top: u8, value: Value) -> Result<()> {
        //eprintln!("replace below {}: {:?} {:?}", from_top, self.stack,);
        let index = self.stack.len() - 1 - from_top as usize;
        *self
            .stack
            .get_mut(index)
            .ok_or("invalid stack replace below")? = value;
        Ok(())
    }

    pub fn pop(&mut self) -> Result<Value> {
        //eprintln!("pop: {:?}", self.stack);
        self.stack.pop().ok_or("invalid stack pop")
    }

    pub fn peek_n(&mut self, n: u8) -> Result<&mut [Value]> {
        let len = self.stack.len();
        if len >= n.into() {
            Ok(&mut self.stack[len - n as usize..])
        } else {
            Err("invalid stack peek n")
        }
    }

    pub fn pop_n(&mut self, n: u8) -> Result<std::vec::Drain<Value>> {
        //eprintln!("pop_n: {} {:?}", n, self.stack);
        Ok(self.stack.drain((self.stack.len() - n as usize)..))
    }
}

impl<'a> IntoIterator for &'a mut OperandStack {
    type Item = &'a mut Value;
    type IntoIter = std::slice::IterMut<'a, Value>;
    fn into_iter(self) -> Self::IntoIter {
        self.stack.iter_mut()
    }
}

pub fn collect_members<F>(
    ctx: &mut VMContext,
    indices: &[ConstantIndex],
    constants: &Constants,
    mut make_value: F,
) -> Result<Value>
where
    F: FnMut(&mut VMContext) -> Result<Value>,
{
    // GCNOTE: unrooted temporary
    // Because `make_value` doesn't allocate, so GC cannot be called in this function.
    // The entries also don't have to be valid until they are initialized, since they
    // won't be observed.
    let mut object_value = ctx.create_object(indices.len());
    let object = object_value.as_object_mut();
    for (i, &constant_index) in indices.iter().enumerate() {
        let member = constants.get(constant_index)?;
        match member {
            &BcObject::Slot(field_index) => {
                // GCNOTE: unrooted temporary from `make_value`: no allocation until the object gets rooted
                object.set_index(i, field_index, FieldValue::Value(make_value(ctx)?));
            }
            BcObject::Method(_) => {
                let method = constants.get(constant_index)?.as_method()?;
                object.set_index(i, method.name, FieldValue::Method(constant_index));
            }
            _ => return Err("invalid object/global member"),
        }
    }
    object.set_parent(make_value(ctx)?);
    Ok(object_value)
}
pub struct VMContext {
    pub frame_stack: FrameStack,
    pub global: Value,
    pub stack: OperandStack,

    mem: *mut u8,
    from_space: usize,
    to_space: usize,
    alloc_ptr: usize,
    scan_ptr: usize,

    heap_log: Option<Box<dyn std::io::Write>>,
}

impl VMContext {
    pub fn new(
        heap_size: usize,
        heap_log: Option<Box<dyn std::io::Write>>,
        globals: &[ConstantIndex],
        constants: &Constants,
    ) -> Result<Self> {
        let heap_size = heap_size * 1_048_576; // MiB
        let layout = alloc::Layout::array::<u8>(heap_size).unwrap();
        let mem = unsafe { alloc::alloc(layout) };

        let mut ctx = VMContext {
            frame_stack: FrameStack::new(),
            global: Value::null(), // to be replaced after initialization
            stack: OperandStack::new(),

            mem,
            from_space: 0,
            to_space: heap_size / 2,
            alloc_ptr: 0,
            scan_ptr: 0, // only used during collection

            heap_log,
        };

        if let Some(heap_log) = &mut ctx.heap_log {
            writeln!(heap_log, "timestamp,event,heap").ok();
            ctx.log("S");
        }

        ctx.global = collect_members(&mut ctx, globals, constants, |_| Ok(Value::Null))?;

        Ok(ctx)
    }

    fn offset(&self, offset: usize) -> *mut u8 {
        unsafe { self.mem.add(offset) }
    }

    fn value_at_offset(&self, offset: usize) -> Value {
        Value::new(self.offset(offset) as *mut GcValueBase)
    }

    fn collect(&mut self) {
        self.alloc_ptr = self.to_space;
        self.scan_ptr = self.to_space;
        mem::swap(&mut self.from_space, &mut self.to_space);

        // Roots
        Self::copy(&mut self.alloc_ptr, self.mem, &mut self.global);
        for local in &mut self.frame_stack {
            Self::copy(&mut self.alloc_ptr, self.mem, local);
        }
        for value in &mut self.stack {
            Self::copy(&mut self.alloc_ptr, self.mem, value);
        }

        while self.scan_ptr < self.alloc_ptr {
            let mut value = self.value_at_offset(self.scan_ptr);
            if let Value::GcValue(gcvalue) = &mut value {
                match gcvalue.tag() {
                    GcValueType::Array => {
                        for element in gcvalue.as_array_mut() {
                            Self::copy(&mut self.alloc_ptr, self.mem, element);
                        }
                    }
                    GcValueType::Object => {
                        let object = gcvalue.as_object_mut();
                        Self::copy(&mut self.alloc_ptr, self.mem, &mut object.parent);
                        for field in object {
                            if let FieldValue::Value(value) = &mut field.value {
                                Self::copy(&mut self.alloc_ptr, self.mem, value);
                            }
                        }
                    }
                    _ => {}
                };
                self.scan_ptr += gcvalue.size();
            }
        }
    }

    fn copy(alloc_ptr: &mut usize, mem: *const u8, value: &mut Value) {
        if let Value::GcValue(gcvalue) = value {
            *gcvalue = if let Some(forwarding_addr) = gcvalue.get_forwarding_addr() {
                forwarding_addr
            } else {
                let size = gcvalue.size();
                let new_addr = GcValue::new(unsafe { mem.add(*alloc_ptr) as *mut GcValueBase });
                *alloc_ptr += size;

                unsafe {
                    ptr::copy_nonoverlapping(gcvalue.as_ptr(), new_addr.as_ptr(), size);

                    ptr::write(gcvalue.as_ptr() as *mut Forwarder, Forwarder::new(new_addr));
                };
                new_addr
            }
        }
    }

    fn allocated(&self) -> usize {
        self.alloc_ptr - self.from_space
    }

    fn available(&self) -> usize {
        self.from_space + self.to_space
    }

    fn log(&mut self, typ: &str) {
        let allocated = self.allocated();
        if let Some(heap_log) = &mut self.heap_log {
            use std::time::SystemTime;
            let time = SystemTime::now()
                .duration_since(SystemTime::UNIX_EPOCH)
                .unwrap()
                .as_nanos();
            writeln!(heap_log, "{},{},{}", time, typ, allocated).ok();
        }
    }

    fn alloc(&mut self, size: usize) -> *mut u8 {
        if self.allocated() + size > self.available() {
            self.collect();
            self.log("G");
        }
        if self.allocated() + size > self.available() {
            panic!("Out of memory.")
        }

        let addr = self.alloc_ptr;
        self.alloc_ptr += size;
        self.log("A");

        self.offset(addr)
    }

    fn create_list<T>(&mut self, value: T, size: usize) -> Value {
        let ptr = self.alloc(size) as *mut T;

        unsafe { ptr::write(ptr, value) };
        Value::new(ptr as *mut GcValueBase)
    }

    pub fn create_bool(&mut self, b: bool) -> Value {
        Value::bool(b)
    }

    pub fn create_int(&mut self, i: i32) -> Value {
        Value::int(i)
    }

    pub fn create_array(&mut self, len: usize) -> Value {
        let array = Array::new(len);
        let size = array.size();
        self.create_list(array, size)
    }

    pub fn create_object(&mut self, field_count: usize) -> Value {
        let object = Object::new(field_count);
        let size = object.size();
        self.create_list(object, size)
    }
}
