pub type Identifier = String;

#[derive(Debug, Clone)]
pub enum Ast {
    // Primitive values
    Integer(i32),
    Boolean(bool),
    Null,

    // Composite values
    Array {
        size: Box<Ast>,
        value: Box<Ast>,
    },
    Object {
        extends: Box<Ast>,
        members: Vec<Ast>,
    },

    // "Expressions"
    VariableDeclaration {
        name: Identifier,
        value: Box<Ast>,
    },
    FunctionDeclaration {
        name: Identifier,
        parameters: Vec<Identifier>,
        body: Box<Ast>,
    },
    FunctionCall {
        name: Identifier,
        arguments: Vec<Ast>,
    },
    MethodCall {
        object: Box<Ast>,
        name: Identifier,
        arguments: Vec<Ast>,
    },
    VariableAccess(Identifier),
    VariableAssignment {
        name: Identifier,
        value: Box<Ast>,
    },
    IndexAccess {
        object: Box<Ast>,
        index: Box<Ast>,
    },
    IndexAssignment {
        object: Box<Ast>,
        index: Box<Ast>,
        value: Box<Ast>,
    },
    FieldAccess {
        object: Box<Ast>,
        field: Identifier,
    },
    FieldAssignment {
        object: Box<Ast>,
        field: Identifier,
        value: Box<Ast>,
    },
    If {
        condition: Box<Ast>,
        consequent: Box<Ast>,
        alternative: Box<Ast>,
    },
    While {
        condition: Box<Ast>,
        body: Box<Ast>,
    },
    Print {
        format: String,
        arguments: Vec<Ast>,
    },
    Block(Vec<Ast>),
}
