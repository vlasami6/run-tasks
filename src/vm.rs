use std::collections::HashMap;

use crate::bytecode::*;
use crate::runtime::*;
use crate::value::*;

type Result<T> = std::result::Result<T, &'static str>;

pub struct VM {
    constants: Constants,
    context: VMContext,
    entry_point: ConstantIndex,
}

impl VM {
    pub fn new(
        mut program: Program,
        heap_size: usize,
        heap_log: Option<Box<dyn std::io::Write>>,
    ) -> Result<VM> {
        // reverse the class members so that we have them in pop friendly order
        for bcobject in &mut program.constants {
            if let BcObject::Class(members) = bcobject {
                members.reverse();
            }
        }
        let mut labels: HashMap<ConstantIndex, usize> = HashMap::new();
        // create a mapping from labels to jump offsets
        for bcobject in &program.constants {
            if let BcObject::Method(method) = bcobject {
                for (i, inst) in method.code.iter().enumerate() {
                    if let &Instruction::Label(label) = inst {
                        if labels.insert(label, i as usize).is_some() {
                            return Err("duplicate label");
                        }
                    }
                }
            }
        }

        let constants = Constants::new(program.constants, labels);
        let context = VMContext::new(heap_size, heap_log, &program.globals, &constants)?;
        Ok(VM {
            constants,
            context,
            entry_point: program.entry_point,
        })
    }

    pub fn run(&mut self) -> Result<()> {
        VM::call_function(&mut self.context, &self.constants, self.entry_point, 0)
    }

    fn call_function(
        ctx: &mut VMContext,
        constants: &Constants,
        method_idx: ConstantIndex,
        argument_cnt: u8,
    ) -> Result<()> {
        let method = constants.get(method_idx)?.as_method()?;

        if argument_cnt != method.argument_cnt {
            return Err("wrong arity");
        }

        let total = LocalIndex(argument_cnt as u16 + method.local_cnt);
        // ctx.frame_stack.push(total);
        // GCNOTE: allocates integer (TODO)
        FrameStack::push(ctx, total);
        // arguments go into locals argument_cnt - 1, argument_cnt - 2, ... 0
        // where 0 is or isn't an object (both cases handled transparently)
        for i in (0..argument_cnt as u16).rev() {
            // GCNOTE: unrooted temporary gets rooted immediately without allocation
            let value = ctx.stack.pop()?;
            ctx.frame_stack.set_local(LocalIndex(i), value)?;
        }
        // the rest of locals are initialized to null
        for i in argument_cnt as u16..total.0 {
            ctx.frame_stack.set_local(LocalIndex(i), Value::Null)?;
        }

        Self::run_function(ctx, constants, &method.code)?;
        ctx.frame_stack.pop(total)?;
        Ok(())
    }

    fn run_function(
        ctx: &mut VMContext,
        constants: &Constants,
        code: &[Instruction],
    ) -> Result<()> {
        let mut ip: usize = 0;
        let end = code.len();
        while ip != end {
            //eprintln!("executing inst. {}: {:?}", ip, method.code[ip]);
            match code[ip] {
                Instruction::Literal(index) => {
                    let literal = constants.get(index)?;
                    // GCNOTE: unrooted temporary gets rooted immediately
                    let value = match *literal {
                        BcObject::Null => Value::Null,
                        BcObject::Boolean(b) => ctx.create_bool(b),
                        BcObject::Integer(i) => ctx.create_int(i),
                        _ => return Err("invalid literal"),
                    };
                    ctx.stack.push(value);
                }

                Instruction::Array => {
                    // GCNOTE: beware, so that initializer doesn't get unrooted
                    let length = ctx
                        .stack
                        .peek_below(1)?
                        .try_as_integer()
                        .map_err(|_| "array length not an integer")?;
                    if length < 0 {
                        return Err("negative array length");
                    }
                    let mut array_obj = ctx.create_array(length as usize);
                    let initializer = ctx.stack.pop()?;
                    ctx.stack.pop()?; // length
                    let array = array_obj.as_array_mut();
                    for i in 0..length as usize {
                        array.set_index(i as i32, initializer)?;
                    }
                    ctx.stack.push(array_obj);
                }

                Instruction::Object(class) => {
                    let class = constants.get(class)?.as_class()?;
                    let object = collect_members(ctx, class, constants, |ctx| ctx.stack.pop())?;
                    ctx.stack.push(object);
                }

                Instruction::GetLocal(index) => {
                    let value = ctx.frame_stack.get_local(index)?;
                    ctx.stack.push(value);
                }

                Instruction::SetLocal(index) => {
                    let value = ctx.stack.peek()?;
                    ctx.frame_stack.set_local(index, value)?;
                }

                Instruction::GetGlobal(index) => {
                    let value = ctx.global.as_object().get_field(index)?;
                    ctx.stack.push(value);
                }

                Instruction::SetGlobal(index) => {
                    let value = ctx.stack.peek()?;
                    ctx.global.as_object_mut().set_field(index, value)?;
                }

                Instruction::GetField(field) => {
                    // GCNOTE: unrooted temporary: OK, no allocation in get_field
                    let object = ctx.stack.pop()?;
                    let field = object.try_as_object()?.get_field(field)?;
                    ctx.stack.push(field);
                }

                Instruction::SetField(field) => {
                    // GCNOTE: unrooted temporaries: OK, no allocation in set_field
                    let value = ctx.stack.pop()?;
                    let mut object = ctx.stack.pop()?;
                    object.try_as_object_mut()?.set_field(field, value)?;
                    ctx.stack.push(value);
                }

                Instruction::Label(_) => {}

                Instruction::Jump(label) => {
                    ip = constants.label_offset(label)?;
                }

                Instruction::Branch(label) => {
                    let value = ctx.stack.pop()?;
                    if value.to_boolean() {
                        ip = constants.label_offset(label)?;
                    }
                }

                Instruction::CallFunction {
                    function,
                    argument_cnt,
                } => {
                    let (_, function) = ctx.global.as_object().get_method(function)?;
                    VM::call_function(ctx, constants, function, argument_cnt)?;
                }

                Instruction::CallMethod {
                    method,
                    argument_cnt,
                } => {
                    let object = ctx.stack.peek_below(argument_cnt - 1)?;
                    let obj_method = object
                        .try_as_object()
                        .and_then(|obj| obj.get_method(method));
                    if let Ok((receiver, method)) = obj_method {
                        // GCNOTE: unrooted temporary: receiver parent, TODO?
                        ctx.stack.replace_below(argument_cnt - 1, receiver)?;
                        VM::call_function(ctx, constants, method, argument_cnt)?;
                    } else {
                        // Including the object (so argument_cnt, not argument_cnt - 1)
                        // GCNOTE: peek_n so that the arguments stay rooted (primitive method call can allocate)
                        let mut arguments = Vec::from(ctx.stack.peek_n(argument_cnt)?);
                        arguments[0] = arguments[0].get_primitive();
                        let method_name = constants.get(method)?.as_string()?;
                        let result = Value::primitive_method_call(ctx, method_name, &arguments)?;
                        // drop the copied arguments
                        drop(ctx.stack.pop_n(argument_cnt).unwrap());
                        ctx.stack.push(result);
                    }
                }

                Instruction::Return => {
                    return Ok(());
                }

                Instruction::Drop => {
                    ctx.stack.pop()?;
                }

                Instruction::Print {
                    format,
                    argument_cnt,
                } => {
                    let mut in_escape = false;
                    let format = constants.get(format)?.as_string()?;
                    // GCNOTE: unrooted values, OK, since no allocation during print
                    let mut args = ctx.stack.pop_n(argument_cnt)?;
                    for c in format.bytes() {
                        if in_escape {
                            in_escape = false;
                            print!(
                                "{}",
                                match c {
                                    b'n' => '\n',
                                    b't' => '\t',
                                    b'r' => '\r',
                                    b'~' => '~',
                                    b'"' => '"',
                                    b'\\' => '\\',
                                    _ => return Err("invalid string escape sequence"),
                                }
                            )
                        } else {
                            match c {
                                b'\\' => in_escape = true,
                                b'~' => print!(
                                    "{}",
                                    DisplayValue(
                                        args.next().ok_or("wrong print arg count")?,
                                        constants
                                    )
                                ),
                                _ => print!("{}", c as char),
                            }
                        }
                    }
                    // Drop Drain iterator so there is no pending mutable borrow of stack
                    drop(args);
                    ctx.stack.push(Value::Null);
                }
            }
            ip += 1;
        }
        Ok(())
        //Err("missing return")
    }
}
