use std::collections::HashMap;
use std::hash::Hash;
use std::marker::PhantomData;
use std::mem;
use std::ops::Index;

use indexmap::IndexSet;

use crate::ast::*;
use crate::bytecode::*;

#[derive(Clone)]
enum EnvironmentEntry {
    Local(LocalIndex),
    Global,
    Function,
}

#[derive(Clone)]
struct Environment(HashMap<Identifier, EnvironmentEntry>);

impl Environment {
    pub fn new() -> Self {
        Self(HashMap::new())
    }

    pub fn define_local(&mut self, name: Identifier, index: LocalIndex) {
        self.0.insert(name, EnvironmentEntry::Local(index));
    }

    pub fn define_global(&mut self, name: Identifier) {
        self.0.insert(name, EnvironmentEntry::Global);
    }

    pub fn define_function(&mut self, name: Identifier) {
        self.0.insert(name, EnvironmentEntry::Function);
    }

    pub fn get_variable(&mut self, name: &str) -> Result<Option<LocalIndex>, &'static str> {
        if let Some(var) = self.0.get(name) {
            Ok(match *var {
                EnvironmentEntry::Local(index) => Some(index),
                EnvironmentEntry::Global => None,
                EnvironmentEntry::Function => return Err("function, not a variable"),
            })
        } else {
            self.define_global(name.to_owned());
            Ok(None)
        }
    }
}

trait Ref {
    fn from_usize(i: usize) -> Self;
    fn to_usize(&self) -> usize;
}

impl Ref for ConstantIndex {
    fn from_usize(i: usize) -> Self {
        Self(i as u16)
    }
    fn to_usize(&self) -> usize {
        self.0 as usize
    }
}

struct UniqueMap<K, V>
where
    K: Ref,
    V: Hash + Eq,
{
    map: IndexSet<V>,
    unused: PhantomData<K>,
}

impl<K, V> UniqueMap<K, V>
where
    K: Ref,
    V: Hash + Eq,
{
    pub fn new() -> Self {
        Self {
            map: IndexSet::new(),
            unused: PhantomData,
        }
    }

    pub fn push(&mut self, v: V) -> K {
        let (index, _) = self.map.insert_full(v);
        K::from_usize(index)
    }

    pub fn into_iter(self) -> indexmap::set::IntoIter<V> {
        self.map.into_iter()
    }
}

impl<K, V> Index<K> for UniqueMap<K, V>
where
    K: Ref,
    V: Hash + Eq,
{
    type Output = V;

    fn index(&self, k: K) -> &V {
        &self.map[k.to_usize()]
    }
}

pub struct Compiler {
    environment: Environment,
    constants: UniqueMap<ConstantIndex, BcObject>,
    current_members: Vec<ConstantIndex>,
    local_cnt: u16,
    current_code: Vec<Instruction>,
    in_block: bool,
    in_object: bool,
    labels: HashMap<ConstantIndex, usize>,
    label_cnt: usize,
}

impl Compiler {
    pub fn new() -> Self {
        Self {
            environment: Environment::new(),
            constants: UniqueMap::new(),
            current_members: Vec::new(),
            current_code: Vec::new(),
            local_cnt: 0,
            in_block: false,
            in_object: false,
            labels: HashMap::new(),
            label_cnt: 0,
        }
    }

    pub fn compile_program(
        mut self,
        name: &str,
        root_nodes: &[Ast],
    ) -> Result<Program, &'static str> {
        let name = self.string(name);

        for n in root_nodes {
            self.compile(n)?;
            self.drop();
        }

        let entry_point = self.constants.push(BcObject::Method(Method {
            name,
            argument_cnt: 0,
            local_cnt: self.local_cnt,
            code: self.current_code,
        }));

        Ok(Program {
            constants: self.constants.into_iter().collect(),
            globals: self.current_members,
            entry_point,
        })
    }

    fn new_local_raw(&mut self) -> LocalIndex {
        let index = self.local_cnt;
        self.local_cnt += 1;
        LocalIndex(index)
    }

    fn new_local(&mut self, name: &str) -> LocalIndex {
        let index = self.new_local_raw();
        self.environment.define_local(name.to_owned(), index);
        index
    }

    fn constant(&mut self, constant: BcObject) -> ConstantIndex {
        self.constants.push(constant)
    }

    fn string(&mut self, string: &str) -> ConstantIndex {
        self.constant(BcObject::String(string.to_owned()))
    }

    fn add_instr(&mut self, instruction: Instruction) {
        self.current_code.push(instruction);
    }

    fn literal(&mut self, constant: BcObject) {
        let constant = self.constant(constant);
        self.add_instr(Instruction::Literal(constant));
    }

    fn drop(&mut self) {
        self.add_instr(Instruction::Drop);
    }

    fn call_method(&mut self, name: &str, argument_cnt: usize) {
        let method = self.string(name);
        self.add_instr(Instruction::CallMethod {
            method,
            argument_cnt: u8::try_from(argument_cnt).expect("too many method arguments"),
        });
    }

    fn label_reserve(&mut self) -> ConstantIndex {
        let label_index = self.label_cnt;
        self.label_cnt += 1;
        let label = self.string(&format!("L{}", label_index));
        label
    }

    fn label_insert(&mut self, label: ConstantIndex) {
        let index = self.current_code.len();
        self.add_instr(Instruction::Label(label));
        self.labels.insert(label, index);
    }

    fn label(&mut self) -> ConstantIndex {
        let label = self.label_reserve();
        self.label_insert(label);
        label
    }

    fn branch(&mut self, label: ConstantIndex) {
        self.add_instr(Instruction::Branch(label));
    }

    fn jump(&mut self, label: ConstantIndex) {
        self.add_instr(Instruction::Jump(label));
    }

    fn compile(&mut self, n: &Ast) -> Result<(), &'static str> {
        match n {
            &Ast::Integer(i) => self.literal(BcObject::Integer(i)),
            &Ast::Boolean(b) => self.literal(BcObject::Boolean(b)),
            Ast::Null => self.literal(BcObject::Null),

            Ast::Array { size, value } => {
                match &**value {
                    Ast::Integer(_) | Ast::Boolean(_) | Ast::Null | Ast::VariableAccess(_) => {
                        // low hanging fruit - for some primitive values the below is not necessary
                        self.compile(size)?;
                        self.compile(value)?;
                        self.add_instr(Instruction::Array);
                        return Ok(());
                    }
                    _ => {}
                }

                let size_var = self.new_local_raw();
                let i_var = self.new_local_raw();
                let array_var = self.new_local_raw();

                self.literal(BcObject::Integer(0));
                self.add_instr(Instruction::SetLocal(i_var));
                self.drop();

                self.compile(size)?;
                self.add_instr(Instruction::SetLocal(size_var));
                // Dummy initializer
                self.literal(BcObject::Null);
                self.add_instr(Instruction::Array);
                self.add_instr(Instruction::SetLocal(array_var));

                let cond_label = self.label();
                let init_label = self.label_reserve();
                let after_label = self.label_reserve();
                self.add_instr(Instruction::GetLocal(i_var));
                self.add_instr(Instruction::GetLocal(size_var));
                self.call_method("<", 2);
                self.branch(init_label);
                self.jump(after_label);
                self.label_insert(init_label);
                self.add_instr(Instruction::GetLocal(i_var));
                self.compile(value)?;
                self.call_method("set", 3);
                self.drop();
                self.add_instr(Instruction::GetLocal(i_var));
                self.literal(BcObject::Integer(1));
                self.call_method("+", 2);
                self.add_instr(Instruction::SetLocal(i_var));
                self.drop();
                self.add_instr(Instruction::GetLocal(array_var));
                self.jump(cond_label);
                self.label_insert(after_label);
            }

            Ast::Object { extends, members } => {
                self.compile(extends)?;

                let saved_members = mem::take(&mut self.current_members);
                let saved_in_object = self.in_object;
                self.in_object = true;
                for member in members {
                    match member {
                        Ast::VariableDeclaration { .. } | Ast::FunctionDeclaration { .. } => {
                            self.compile(member)?;
                        }
                        _ => return Err("invalid object member"),
                    }
                }
                self.in_object = saved_in_object;
                let object_members = mem::replace(&mut self.current_members, saved_members);

                let class = self.constant(BcObject::Class(object_members));
                self.add_instr(Instruction::Object(class));
            }

            Ast::VariableDeclaration { name, value } => {
                self.compile(value)?;
                if self.in_object {
                    let name_const = self.string(name);
                    let slot = self.constant(BcObject::Slot(name_const));
                    self.current_members.push(slot);
                } else if !self.in_block {
                    let name_const = self.string(name);
                    let slot = self.constant(BcObject::Slot(name_const));
                    self.current_members.push(slot);
                    self.environment.define_global(name.clone());
                    self.add_instr(Instruction::SetGlobal(name_const));
                } else {
                    let index = self.new_local(name);
                    self.add_instr(Instruction::SetLocal(index))
                }
            }

            Ast::FunctionDeclaration {
                name,
                parameters,
                body,
            } => {
                self.environment.define_function(name.clone());
                let name = self.string(name);

                let saved_env = self.environment.clone();
                let saved_code = mem::take(&mut self.current_code);
                let saved_local_cnt = self.local_cnt;
                let saved_in_block = self.in_block;
                self.in_block = true;
                self.local_cnt = 0;
                if self.in_object {
                    self.new_local("this");
                }
                for param in parameters {
                    self.new_local(param);
                }
                let saved_in_object = self.in_object;
                self.in_object = false;
                self.compile(body)?;
                self.add_instr(Instruction::Return);
                let method_code = mem::replace(&mut self.current_code, saved_code);
                self.environment = saved_env;

                let method = self.constant(BcObject::Method(Method {
                    name,
                    argument_cnt: u8::try_from(parameters.len() + saved_in_object as usize)
                        .expect("too many function parameters"),
                    local_cnt: self.local_cnt,
                    code: method_code,
                }));

                self.in_object = saved_in_object;
                self.in_block = saved_in_block;
                self.local_cnt = saved_local_cnt;

                self.current_members.push(method);
                if !self.in_object {
                    self.literal(BcObject::Null);
                }
            }

            Ast::FunctionCall { name, arguments } => {
                let function = self.string(name);

                for arg in arguments {
                    self.compile(arg)?;
                }

                self.add_instr(Instruction::CallFunction {
                    function,
                    argument_cnt: u8::try_from(arguments.len())
                        .expect("too many function arguments"),
                });
            }

            Ast::MethodCall {
                object,
                name,
                arguments,
            } => {
                self.compile(object)?;
                for arg in arguments {
                    self.compile(arg)?;
                }
                self.call_method(name, arguments.len() + 1);
            }

            Ast::VariableAccess(name) => match self.environment.get_variable(name)? {
                Some(index) => self.add_instr(Instruction::GetLocal(index)),
                None => {
                    let name = self.string(name);
                    self.add_instr(Instruction::GetGlobal(name))
                }
            },
            Ast::VariableAssignment { name, value } => {
                self.compile(value)?;
                match self.environment.get_variable(name)? {
                    Some(index) => self.add_instr(Instruction::SetLocal(index)),
                    None => {
                        let name = self.string(name);
                        self.add_instr(Instruction::SetGlobal(name))
                    }
                };
            }

            Ast::IndexAccess { object, index } => {
                self.compile(object)?;
                self.compile(index)?;
                self.call_method("get", 2);
            }
            Ast::IndexAssignment {
                object,
                index,
                value,
            } => {
                self.compile(object)?;
                self.compile(index)?;
                self.compile(value)?;
                self.call_method("set", 3);
            }

            Ast::FieldAccess { object, field } => {
                let field = self.string(field);
                self.compile(object)?;
                self.add_instr(Instruction::GetField(field));
            }
            Ast::FieldAssignment {
                object,
                field,
                value,
            } => {
                let field = self.string(field);
                self.compile(object)?;
                self.compile(value)?;
                self.add_instr(Instruction::SetField(field));
            }

            Ast::If {
                condition,
                consequent,
                alternative,
            } => {
                let conseq_label = self.label_reserve();
                let altern_label = self.label_reserve();
                let after_label = self.label_reserve();
                self.compile(condition)?;
                self.branch(conseq_label);
                self.jump(altern_label);
                self.label_insert(conseq_label);
                self.compile(consequent)?;
                self.jump(after_label);
                self.label_insert(altern_label);
                self.compile(alternative)?;
                self.label_insert(after_label);
            }

            Ast::While { condition, body } => {
                self.literal(BcObject::Null);
                let cond_label = self.label();
                let body_label = self.label_reserve();
                let after_label = self.label_reserve();
                self.compile(condition)?;
                self.branch(body_label);
                self.jump(after_label);
                self.label_insert(body_label);
                // Drop value from previous iteration (or the initial null)
                self.drop();
                self.compile(body)?;
                self.jump(cond_label);
                self.label_insert(after_label);
            }

            Ast::Print { format, arguments } => {
                let format = self.string(format);
                for arg in arguments {
                    self.compile(arg)?;
                }
                self.add_instr(Instruction::Print {
                    format,
                    argument_cnt: u8::try_from(arguments.len()).expect("too many print parameters"),
                })
            }

            Ast::Block(nodes) => {
                let saved_env = self.environment.clone();
                let saved_in_block = self.in_block;
                self.in_block = true;
                for (i, n) in nodes.iter().enumerate() {
                    self.compile(n)?;
                    if i != nodes.len() - 1 {
                        self.drop();
                    };
                }
                self.in_block = saved_in_block;
                self.environment = saved_env;
            }
        };

        Ok(())
    }
}
