use crate::ast::*;
use crate::lexer::{Lexer, Token, TokenKind};

pub struct Parser<'a> {
    lexer: Lexer<'a>,
    lookahead: Token,
    in_object: bool,
}

impl<'a> Parser<'a> {
    pub fn new(input: &'a str) -> Self {
        let mut lexer = Lexer::new(input);
        let lookahead = lexer.next();
        Self {
            lexer,
            lookahead,
            in_object: false,
        }
    }

    pub fn parse(&mut self) -> Result<Vec<Ast>, &'static str> {
        let result = self.parse_program();
        match result {
            Ok(nodes) => Ok(nodes),
            Err(e) => {
                let (linenr, colnr) = (self.lookahead.loc.line, self.lookahead.loc.col);
                eprintln!(
                    "parser error: {} at [{},{}]: {:?}",
                    e, linenr, colnr, self.lookahead.kind
                );
                let line = self.lexer.buffer.lines().nth(linenr).unwrap_or("");
                eprintln!("{}", line);
                eprintln!("{}^", " ".repeat(colnr));
                Err(e)
            }
        }
    }

    fn parse_program(&mut self) -> Result<Vec<Ast>, &'static str> {
        let expressions = self.parse_expression_list(TokenKind::Semicolon, TokenKind::Eof)?;
        self.eat_token(TokenKind::Eof, "expected end of file")?;
        Ok(expressions)
    }

    fn peek_token(&self) -> &TokenKind {
        //println!("peek token: {:?}", self.lookahead.kind);
        &self.lookahead.kind
    }

    fn discard_token(&mut self) -> Result<Token, &'static str> {
        let a = std::mem::replace(&mut self.lookahead, self.lexer.next());
        if let TokenKind::Error(err) = a.kind {
            return Err(err);
        }
        Ok(a)
    }

    fn eat_token(&mut self, kind: TokenKind, err: &'static str) -> Result<Token, &'static str> {
        let tok = self.discard_token()?;
        match tok.kind == kind {
            true => Ok(tok),
            false => {
                // restore token for error message
                self.lookahead = tok;
                Err(err)
            }
        }
    }

    fn eat_identifier(&mut self) -> Result<Identifier, &'static str> {
        let tok = self.discard_token()?;
        match tok.kind {
            TokenKind::Identifier(ident) => Ok(ident),
            _ => {
                self.lookahead = tok;
                Err("expected identifier")
            }
        }
    }

    fn eat_string(&mut self) -> Result<String, &'static str> {
        let tok = self.discard_token()?;
        match tok.kind {
            TokenKind::String(string) => Ok(string),
            _ => {
                self.lookahead = tok;
                Err("expected string")
            }
        }
    }

    fn parse_identifier_list(&mut self) -> Result<Vec<Identifier>, &'static str> {
        let mut identifiers = Vec::new();

        if *self.peek_token() != TokenKind::RParen {
            loop {
                identifiers.push(self.eat_identifier()?);
                if let TokenKind::Comma = self.peek_token() {
                    self.discard_token()?;
                }
                if let TokenKind::RParen = self.peek_token() {
                    break;
                }
            }
        }
        self.discard_token()?;

        Ok(identifiers)
    }

    fn parse_expression_list(
        &mut self,
        separator: TokenKind,
        terminator: TokenKind,
    ) -> Result<Vec<Ast>, &'static str> {
        let mut expressions = Vec::new();

        if *self.peek_token() != terminator {
            loop {
                expressions.push(self.parse_expression()?);
                if *self.peek_token() == separator {
                    self.discard_token()?;
                }
                if *self.peek_token() == terminator {
                    break;
                }
            }
        }
        self.discard_token()?;

        Ok(expressions)
    }

    fn parse_expression(&mut self) -> Result<Ast, &'static str> {
        self.parse_expression_bp(0)
    }

    fn parse_expression_bp(&mut self, bp: i8) -> Result<Ast, &'static str> {
        //println!("expression start: {:?}", self.peek_token());
        // TODO: peek for nud instead of discard
        let token = self.discard_token()?;
        let mut left = match token.kind {
            TokenKind::Number(n) => Ast::Integer(n),
            TokenKind::True => Ast::Boolean(true),
            TokenKind::False => Ast::Boolean(false),
            TokenKind::Null => Ast::Null,
            TokenKind::Identifier(ident) => Ast::VariableAccess(ident),

            TokenKind::Begin => self.null_block()?,
            TokenKind::Let => self.null_let()?,
            TokenKind::Function => self.null_function()?,
            TokenKind::Array => self.null_array()?,
            TokenKind::Object => self.null_object()?,
            TokenKind::If => self.null_if()?,
            TokenKind::While => self.null_while()?,
            TokenKind::Print => self.null_print()?,
            TokenKind::LParen => self.null_parenthesis()?,

            TokenKind::Semicolon => Ast::Null, // TODO: keep this?
            _ => {
                // TODO: a better way to improve error messages
                self.lookahead = token;
                return Err("unexpected token at start of expression");
            }
        };

        loop {
            type LedFunc<'a, 'b> =
                fn(&'a mut Parser<'b>, Ast, i8, TokenKind) -> Result<Ast, &'static str>;
            let (led, lbp, rbp): (LedFunc, i8, i8) = match self.peek_token() {
                TokenKind::Op("|") => (Self::left_binop, 3, 4),
                TokenKind::Op("&") => (Self::left_binop, 5, 6),
                TokenKind::Op("==") => (Self::left_binop, 7, 8),
                TokenKind::Op("!=") => (Self::left_binop, 7, 8),
                TokenKind::Op(">") => (Self::left_binop, 7, 8),
                TokenKind::Op("<") => (Self::left_binop, 7, 8),
                TokenKind::Op(">=") => (Self::left_binop, 7, 8),
                TokenKind::Op("<=") => (Self::left_binop, 7, 8),
                TokenKind::Op("+") => (Self::left_binop, 9, 10),
                TokenKind::Op("-") => (Self::left_binop, 9, 10),
                TokenKind::Op("/") => (Self::left_binop, 11, 12),
                TokenKind::Op("*") => (Self::left_binop, 11, 12),
                TokenKind::Op("%") => (Self::left_binop, 11, 12),
                TokenKind::LParen => (Self::left_call, 13, 14),
                TokenKind::LBracket => (Self::left_index, 13, 14),
                TokenKind::Dot => (Self::left_field, 13, 14),
                TokenKind::LArrow => (Self::left_assign, 2, 1),
                _ => return Ok(left),
                //_ => return Err("invalid binary operator"),
            };

            if lbp < bp {
                break;
            }

            let token_kind = self.discard_token()?.kind;
            left = led(self, left, rbp, token_kind)?;
        }

        Ok(left)
    }

    fn null_block(&mut self) -> Result<Ast, &'static str> {
        let in_object = self.in_object;
        self.in_object = false;
        let result = Ok(Ast::Block(
            self.parse_expression_list(TokenKind::Semicolon, TokenKind::End)?,
        ));
        self.in_object = in_object;
        result
    }

    fn null_let(&mut self) -> Result<Ast, &'static str> {
        let name = self.eat_identifier()?;

        self.eat_token(TokenKind::Equal, "expected '=' in variable declaration")?;

        let value = Box::new(self.parse_expression()?);

        Ok(Ast::VariableDeclaration { name, value })
    }

    fn null_function(&mut self) -> Result<Ast, &'static str> {
        let tok = self.discard_token()?;
        let name = match tok.kind {
            TokenKind::Identifier(ident) => ident,
            TokenKind::Op(op) => {
                if self.in_object {
                    op.to_owned()
                } else {
                    return Err("operator methods only allowed in objects");
                }
            }
            _ => {
                self.lookahead = tok;
                return Err("expected function/method name");
            }
        };

        self.eat_token(TokenKind::LParen, "expected '(' to follow function name")?;

        let parameters = self.parse_identifier_list()?;

        self.eat_token(TokenKind::RArrow, "expected '->' after function parameters")?;

        let body = Box::new(self.parse_expression()?);

        Ok(Ast::FunctionDeclaration {
            name,
            parameters,
            body,
        })
    }

    fn null_array(&mut self) -> Result<Ast, &'static str> {
        self.eat_token(TokenKind::LParen, "expected '(' after array keywrod")?;

        let size = Box::new(self.parse_expression()?);

        self.eat_token(TokenKind::Comma, "expected ',' after array size")?;

        let value = Box::new(self.parse_expression()?);

        self.eat_token(TokenKind::RParen, "expected ')' after array initializator")?;

        Ok(Ast::Array { size, value })
    }

    fn null_object(&mut self) -> Result<Ast, &'static str> {
        let extends = Box::new(match self.peek_token() {
            TokenKind::Extends => {
                self.discard_token()?;
                self.parse_expression()?
            }
            _ => Ast::Null,
        });

        self.eat_token(
            TokenKind::Begin,
            "expected objects declarations to start with 'begin'",
        )?;

        self.in_object = true;
        let members = self.parse_expression_list(TokenKind::Semicolon, TokenKind::End)?;
        self.in_object = false;

        Ok(Ast::Object { extends, members })
    }

    fn null_if(&mut self) -> Result<Ast, &'static str> {
        let condition = Box::new(self.parse_expression()?);

        self.eat_token(TokenKind::Then, "expected 'then' after if condition")?;

        let consequent = Box::new(self.parse_expression()?);

        let alternative = Box::new(match self.peek_token() {
            &TokenKind::Else => {
                self.discard_token()?;
                self.parse_expression()?
            }
            _ => Ast::Null,
        });

        Ok(Ast::If {
            condition,
            consequent,
            alternative,
        })
    }

    fn null_while(&mut self) -> Result<Ast, &'static str> {
        let condition = Box::new(self.parse_expression()?);

        self.eat_token(TokenKind::Do, "expected 'do' after while's condition")?;

        let body = Box::new(self.parse_expression()?);

        Ok(Ast::While { condition, body })
    }

    fn null_print(&mut self) -> Result<Ast, &'static str> {
        self.eat_token(TokenKind::LParen, "expected '(' after print")?;
        let format = self.eat_string()?;

        if let TokenKind::Comma = self.peek_token() {
            self.discard_token()?;
        }

        let arguments = self.parse_expression_list(TokenKind::Comma, TokenKind::RParen)?;

        Ok(Ast::Print { format, arguments })
    }

    fn null_parenthesis(&mut self) -> Result<Ast, &'static str> {
        let expr = self.parse_expression()?;

        self.eat_token(
            TokenKind::RParen,
            "expected parenthesised expression to end with ')'",
        )?;

        Ok(expr)
    }

    fn left_binop(&mut self, left: Ast, rbp: i8, kind: TokenKind) -> Result<Ast, &'static str> {
        let op = match kind {
            TokenKind::Op(op) => op,
            _ => unreachable!(),
        };

        let right = self.parse_expression_bp(rbp)?;

        Ok(Ast::MethodCall {
            object: Box::new(left),
            name: op.to_string(),
            arguments: vec![right],
        })
    }

    fn left_call(&mut self, left: Ast, _rbp: i8, _kind: TokenKind) -> Result<Ast, &'static str> {
        match left {
            Ast::VariableAccess(_) | Ast::FieldAccess { .. } => {}
            _ => return Err("invalid function/method call"),
        }

        let arguments = self.parse_expression_list(TokenKind::Comma, TokenKind::RParen)?;

        Ok(match left {
            Ast::VariableAccess(name) => Ast::FunctionCall { name, arguments },
            Ast::FieldAccess { object, field } => Ast::MethodCall {
                object,
                name: field,
                arguments,
            },
            _ => unreachable!(),
        })
    }

    fn left_index(&mut self, left: Ast, _rbp: i8, _kind: TokenKind) -> Result<Ast, &'static str> {
        let right = self.parse_expression()?;

        self.eat_token(TokenKind::RBracket, "expected indexing to end with ']'")?;

        Ok(Ast::IndexAccess {
            object: Box::new(left),
            index: Box::new(right),
        })
    }

    fn left_field(&mut self, left: Ast, _rbp: i8, _kind: TokenKind) -> Result<Ast, &'static str> {
        let field = self.eat_identifier()?;

        Ok(Ast::FieldAccess {
            object: Box::new(left),
            field,
        })
    }

    fn left_assign(&mut self, left: Ast, rbp: i8, _kind: TokenKind) -> Result<Ast, &'static str> {
        let right = self.parse_expression_bp(rbp)?;

        Ok(match left {
            Ast::VariableAccess(name) => Ast::VariableAssignment {
                name,
                value: Box::new(right),
            },
            Ast::FieldAccess { object, field } => Ast::FieldAssignment {
                object,
                field,
                value: Box::new(right),
            },
            Ast::IndexAccess { object, index } => Ast::IndexAssignment {
                object,
                index,
                value: Box::new(right),
            },
            _ => return Err("invalid assignment"),
        })
    }
}
