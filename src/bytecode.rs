use std::io::{Error, ErrorKind, Read, Write};

use byteorder::{ReadBytesExt, WriteBytesExt, LE};

type VMResult<T> = std::result::Result<T, &'static str>;

#[derive(Debug, Copy, Clone, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct ConstantIndex(pub u16);

#[derive(Debug, Copy, Clone, PartialEq, Eq, Hash)]
pub struct LocalIndex(pub u16);

#[derive(Debug)]
pub struct Program {
    pub constants: Vec<BcObject>,
    pub globals: Vec<ConstantIndex>,
    pub entry_point: ConstantIndex,
}

#[derive(Debug, Clone, PartialEq, Eq, Hash)]
pub struct Method {
    pub name: ConstantIndex,
    pub argument_cnt: u8,
    pub local_cnt: u16,
    pub code: Vec<Instruction>,
}

#[derive(Debug, PartialEq, Eq, Hash)]
pub enum BcObject {
    Null,
    Boolean(bool),
    Integer(i32),
    String(String),
    Slot(ConstantIndex),
    Method(Method),
    Class(Vec<ConstantIndex>),
}

impl BcObject {
    pub fn as_method(&self) -> VMResult<&Method> {
        if let Self::Method(v) = self {
            Ok(v)
        } else {
            Err("expected method")
        }
    }

    pub fn as_string(&self) -> VMResult<&String> {
        if let Self::String(v) = self {
            Ok(v)
        } else {
            Err("expected string")
        }
    }

    pub fn as_class(&self) -> VMResult<&Vec<ConstantIndex>> {
        if let Self::Class(v) = self {
            Ok(v)
        } else {
            Err("expected class")
        }
    }
}

#[derive(Debug, Copy, Clone, PartialEq, Eq, Hash)]
pub enum Instruction {
    Literal(ConstantIndex),
    GetLocal(LocalIndex),
    SetLocal(LocalIndex),
    GetGlobal(ConstantIndex),
    SetGlobal(ConstantIndex),
    CallFunction {
        function: ConstantIndex,
        argument_cnt: u8,
    },
    Return,
    Label(ConstantIndex),
    Jump(ConstantIndex),
    Branch(ConstantIndex),
    Print {
        format: ConstantIndex,
        argument_cnt: u8,
    },
    Array,
    Object(ConstantIndex),
    GetField(ConstantIndex),
    SetField(ConstantIndex),
    CallMethod {
        method: ConstantIndex,
        argument_cnt: u8,
    },
    Drop,
}

pub trait BinaryRead<R>
where
    R: Read,
    Self: Sized,
{
    fn read(reader: &mut R) -> Result<Self, Error>;
}

pub trait BinaryWrite<W>
where
    W: Write,
    Self: Sized,
{
    fn write(&self, writer: &mut W) -> Result<(), Error>;
}

impl<R> BinaryRead<R> for Program
where
    R: Read,
{
    fn read(reader: &mut R) -> Result<Self, Error>
    where
        R: Read,
    {
        let constant_count = reader.read_u16::<LE>()?;
        let mut constants = Vec::new();
        for _ in 0..constant_count {
            constants.push(BcObject::read(reader)?);
        }

        let global_count = reader.read_u16::<LE>()?;
        let mut globals = Vec::new();
        for _ in 0..global_count {
            globals.push(ConstantIndex(reader.read_u16::<LE>()?));
        }

        let entry_point = ConstantIndex(reader.read_u16::<LE>()?);

        Ok(Program {
            constants,
            globals,
            entry_point,
        })
    }
}

impl<W> BinaryWrite<W> for Program
where
    W: Write,
{
    fn write(&self, writer: &mut W) -> Result<(), Error>
    where
        W: Write,
    {
        writer.write_u16::<LE>(
            self.constants
                .len()
                .try_into()
                .map_err(|_| Error::new(ErrorKind::Other, "too many constants"))?,
        )?;
        for constant in &self.constants {
            constant.write(writer)?
        }

        // number of globals is smaller than the number of constants which is checked above
        writer.write_u16::<LE>(self.globals.len().try_into().unwrap())?;
        for global in &self.globals {
            writer.write_u16::<LE>(global.0)?;
        }

        writer.write_u16::<LE>(self.entry_point.0)?;

        Ok(())
    }
}

impl<R> BinaryRead<R> for BcObject
where
    R: Read,
{
    fn read(reader: &mut R) -> Result<BcObject, Error>
    where
        R: Read,
    {
        let tag = reader.read_u8()?;
        Ok(match tag {
            INTEGER => BcObject::Integer(reader.read_i32::<LE>()?),
            BOOLEAN => BcObject::Boolean(match reader.read_u8()? {
                0 => false,
                1 => true,
                _ => return Err(Error::new(ErrorKind::Other, "invalid boolean")),
            }),
            NULL => BcObject::Null,
            STRING => {
                let length = reader.read_u32::<LE>()?;
                let mut string = String::with_capacity(length as usize);
                for _ in 0..length {
                    let c = reader.read_u8()?;
                    string.push(c as char);
                }
                BcObject::String(string)
            }
            SLOT => BcObject::Slot(ConstantIndex(reader.read_u16::<LE>()?)),
            METHOD => {
                let name = ConstantIndex(reader.read_u16::<LE>()?);
                let argument_cnt = reader.read_u8()?;
                let local_cnt = reader.read_u16::<LE>()?;
                let inst_cnt = reader.read_u32::<LE>()?;
                let mut code = Vec::new();
                for _ in 0..inst_cnt {
                    let instruction = Instruction::read(reader)?;
                    code.push(instruction);
                }
                BcObject::Method(Method {
                    name,
                    argument_cnt,
                    local_cnt,
                    code,
                })
            }
            CLASS => {
                let mut indices = Vec::new();
                let cnt = reader.read_u16::<LE>()?;
                for _ in 0..cnt {
                    indices.push(ConstantIndex(reader.read_u16::<LE>()?));
                }
                BcObject::Class(indices)
            }
            _ => {
                return Err(Error::new(
                    ErrorKind::Other,
                    format!("invalid object tag '{}'", tag),
                ))
            }
        })
    }
}

impl<W> BinaryWrite<W> for BcObject
where
    W: Write,
{
    fn write(&self, writer: &mut W) -> Result<(), Error>
    where
        W: Write,
    {
        match self {
            &BcObject::Integer(i) => {
                writer.write_u8(INTEGER)?;
                writer.write_i32::<LE>(i)?;
            }
            &BcObject::Boolean(b) => {
                writer.write_u8(BOOLEAN)?;
                writer.write_u8(b as u8)?;
            }
            BcObject::Null => {
                writer.write_u8(NULL)?;
            }
            BcObject::String(s) => {
                writer.write_u8(STRING)?;
                writer.write_u32::<LE>(
                    s.len()
                        .try_into()
                        .map_err(|_| Error::new(ErrorKind::Other, "too long string"))?,
                )?;
                writer.write_all(s.as_bytes())?;
            }
            BcObject::Slot(s) => {
                writer.write_u8(SLOT)?;
                writer.write_u16::<LE>(s.0)?;
            }
            BcObject::Method(m) => {
                writer.write_u8(METHOD)?;
                writer.write_u16::<LE>(m.name.0)?;
                writer.write_u8(m.argument_cnt)?;
                writer.write_u16::<LE>(m.local_cnt)?;
                writer.write_u32::<LE>(
                    m.code
                        .len()
                        .try_into()
                        .map_err(|_| Error::new(ErrorKind::Other, "too many instructions"))?,
                )?;
                for instruction in &m.code {
                    instruction.write(writer)?;
                }
            }
            BcObject::Class(c) => {
                writer.write_u8(CLASS)?;
                writer.write_u16::<LE>(
                    c.len()
                        .try_into()
                        .map_err(|_| Error::new(ErrorKind::Other, "too many class members"))?,
                )?;
                for member in c {
                    writer.write_u16::<LE>(member.0)?
                }
            }
        }
        Ok(())
    }
}

impl<R> BinaryRead<R> for Instruction
where
    R: Read,
{
    fn read(reader: &mut R) -> Result<Instruction, Error>
    where
        R: Read,
    {
        let tag = reader.read_u8()?;
        Ok(match tag {
            LITERAL => Instruction::Literal(ConstantIndex(reader.read_u16::<LE>()?)),
            GET_LOCAL => Instruction::GetLocal(LocalIndex(reader.read_u16::<LE>()?)),
            SET_LOCAL => Instruction::SetLocal(LocalIndex(reader.read_u16::<LE>()?)),
            GET_GLOBAL => Instruction::GetGlobal(ConstantIndex(reader.read_u16::<LE>()?)),
            SET_GLOBAL => Instruction::SetGlobal(ConstantIndex(reader.read_u16::<LE>()?)),
            CALL_FUNCTION => {
                let function = ConstantIndex(reader.read_u16::<LE>()?);
                let argument_cnt = reader.read_u8()?;
                Instruction::CallFunction {
                    function,
                    argument_cnt,
                }
            }
            RETURN => Instruction::Return,
            LABEL => Instruction::Label(ConstantIndex(reader.read_u16::<LE>()?)),
            JUMP => Instruction::Jump(ConstantIndex(reader.read_u16::<LE>()?)),
            BRANCH => Instruction::Branch(ConstantIndex(reader.read_u16::<LE>()?)),
            PRINT => {
                let format = ConstantIndex(reader.read_u16::<LE>()?);
                let argument_cnt = reader.read_u8()?;
                Instruction::Print {
                    format,
                    argument_cnt,
                }
            }
            ARRAY => Instruction::Array,
            OBJECT => Instruction::Object(ConstantIndex(reader.read_u16::<LE>()?)),
            GET_FIELD => Instruction::GetField(ConstantIndex(reader.read_u16::<LE>()?)),
            SET_FIELD => Instruction::SetField(ConstantIndex(reader.read_u16::<LE>()?)),
            CALL_METHOD => {
                let method = ConstantIndex(reader.read_u16::<LE>()?);
                let argument_cnt = reader.read_u8()?;
                Instruction::CallMethod {
                    method,
                    argument_cnt,
                }
            }
            DROP => Instruction::Drop,
            _ => {
                return Err(Error::new(
                    ErrorKind::Other,
                    format!("invalid instruction tag '{}'", tag),
                ))
            }
        })
    }
}

impl<W> BinaryWrite<W> for Instruction
where
    W: Write,
{
    fn write(&self, writer: &mut W) -> Result<(), Error>
    where
        W: Write,
    {
        match self {
            Instruction::Literal(idx) => {
                writer.write_u8(LITERAL)?;
                writer.write_u16::<LE>(idx.0)?;
            }
            Instruction::GetLocal(idx) => {
                writer.write_u8(GET_LOCAL)?;
                writer.write_u16::<LE>(idx.0)?;
            }
            Instruction::SetLocal(idx) => {
                writer.write_u8(SET_LOCAL)?;
                writer.write_u16::<LE>(idx.0)?;
            }
            Instruction::GetGlobal(idx) => {
                writer.write_u8(GET_GLOBAL)?;
                writer.write_u16::<LE>(idx.0)?;
            }
            Instruction::SetGlobal(idx) => {
                writer.write_u8(SET_GLOBAL)?;
                writer.write_u16::<LE>(idx.0)?;
            }
            Instruction::CallFunction {
                function,
                argument_cnt,
            } => {
                writer.write_u8(CALL_FUNCTION)?;
                writer.write_u16::<LE>(function.0)?;
                writer.write_u8(*argument_cnt)?;
            }
            Instruction::Return => {
                writer.write_u8(RETURN)?;
            }
            Instruction::Label(idx) => {
                writer.write_u8(LABEL)?;
                writer.write_u16::<LE>(idx.0)?;
            }
            Instruction::Jump(idx) => {
                writer.write_u8(JUMP)?;
                writer.write_u16::<LE>(idx.0)?;
            }
            Instruction::Branch(idx) => {
                writer.write_u8(BRANCH)?;
                writer.write_u16::<LE>(idx.0)?;
            }
            Instruction::Print {
                format,
                argument_cnt,
            } => {
                writer.write_u8(PRINT)?;
                writer.write_u16::<LE>(format.0)?;
                writer.write_u8(*argument_cnt)?;
            }
            Instruction::Array => {
                writer.write_u8(ARRAY)?;
            }
            Instruction::Object(idx) => {
                writer.write_u8(OBJECT)?;
                writer.write_u16::<LE>(idx.0)?;
            }
            Instruction::GetField(idx) => {
                writer.write_u8(GET_FIELD)?;
                writer.write_u16::<LE>(idx.0)?;
            }
            Instruction::SetField(idx) => {
                writer.write_u8(SET_FIELD)?;
                writer.write_u16::<LE>(idx.0)?;
            }
            Instruction::CallMethod {
                method,
                argument_cnt,
            } => {
                writer.write_u8(CALL_METHOD)?;
                writer.write_u16::<LE>(method.0)?;
                writer.write_u8(*argument_cnt)?;
            }
            Instruction::Drop => {
                writer.write_u8(DROP)?;
            }
        }
        Ok(())
    }
}

const INTEGER: u8 = 0x00;
const BOOLEAN: u8 = 0x06;
const NULL: u8 = 0x01;
const STRING: u8 = 0x02;
const SLOT: u8 = 0x04;
const METHOD: u8 = 0x03;
const CLASS: u8 = 0x05;

const LITERAL: u8 = 0x01;
const GET_LOCAL: u8 = 0x0A;
const SET_LOCAL: u8 = 0x09;
const GET_GLOBAL: u8 = 0x0C;
const SET_GLOBAL: u8 = 0x0B;
const CALL_FUNCTION: u8 = 0x08;
const RETURN: u8 = 0x0F;
const LABEL: u8 = 0x00;
const JUMP: u8 = 0x0E;
const BRANCH: u8 = 0x0D;
const PRINT: u8 = 0x02;
const ARRAY: u8 = 0x03;
const OBJECT: u8 = 0x04;
const GET_FIELD: u8 = 0x05;
const SET_FIELD: u8 = 0x06;
const CALL_METHOD: u8 = 0x07;
const DROP: u8 = 0x10;
