use std::fmt::Display;
use std::ops::{Index, IndexMut};
use std::{mem, ptr, slice};

use crate::bytecode::*;
use crate::runtime::*;

type Result<T> = std::result::Result<T, &'static str>;

#[derive(Debug)]
pub enum ValueType {
    Null,

    Boolean,
    Integer,
    Array,
    Object,

    Forwarder,
}

#[derive(Debug)]
#[repr(u64)]
pub enum GcValueType {
    Array,
    Object,

    Forwarder,
}

//const INT_TAG: usize = 0;
//const NULL_TAG: usize = 1;
//const BOOL_TAG: usize = 2;
//const GC_TAG: usize = 0; // 3
//const TAG_BITS: usize = 2;

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum Value {
    Null,
    Boolean(bool),
    Integer(i32),
    GcValue(GcValue),
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub struct GcValue(*mut GcValueBase);

impl Value {
    pub fn new(ptr: *mut GcValueBase) -> Self {
        //Self((ptr as usize | GC_TAG) as *mut ValueBase)
        Self::GcValue(GcValue::new(ptr))
    }

    pub fn null() -> Self {
        //Self(NULL_TAG as *mut ValueBase)
        Self::Null
    }

    pub fn bool(b: bool) -> Self {
        //Self(((b as usize) << TAG_BITS | BOOL_TAG) as *mut ValueBase)
        Self::Boolean(b)
    }

    pub fn int(i: i32) -> Self {
        //Self(((i as usize) << TAG_BITS | INT_TAG) as *mut ValueBase)
        Self::Integer(i)
    }

    pub fn typ(&self) -> ValueType {
        match self {
            Value::Null => ValueType::Null,
            Value::Boolean(_) => ValueType::Boolean,
            Value::Integer(_) => ValueType::Integer,
            Value::GcValue(gcvalue) => match gcvalue.tag() {
                GcValueType::Array => ValueType::Array,
                GcValueType::Object => ValueType::Object,
                GcValueType::Forwarder => ValueType::Forwarder,
            },
        }
    }

    pub fn is_null(&self) -> bool {
        matches!(*self, Value::Null)
    }

    pub fn to_boolean(self) -> bool {
        match self {
            Value::Null => false,
            Value::Boolean(b) => b,
            _ => true,
        }
    }

    pub fn as_gcvalue(&self) -> &GcValue {
        if let Value::GcValue(gcvalue) = self {
            return gcvalue;
        }
        unreachable!()
    }

    pub fn as_gcvalue_mut(&mut self) -> &mut GcValue {
        if let Value::GcValue(gcvalue) = self {
            return gcvalue;
        }
        unreachable!()
    }

    pub fn as_boolean(&self) -> bool {
        match self {
            Value::Boolean(b) => *b,
            _ => unreachable!(),
        }
    }

    pub fn as_integer(&self) -> i32 {
        match self {
            &Value::Integer(i) => i,
            _ => unreachable!(),
        }
    }

    pub fn try_as_integer(&self) -> Result<i32> {
        match self {
            &Value::Integer(i) => Ok(i),
            _ => Err("not an integer"),
        }
    }

    pub fn as_array(&self) -> &Array {
        self.as_gcvalue().as_array()
    }

    pub fn try_as_array(&self) -> Result<&Array> {
        if let Value::GcValue(gcvalue) = self {
            if let GcValueType::Array = gcvalue.tag() {
                return Ok(gcvalue.as_array());
            }
        }
        Err("not array")
    }

    pub fn as_array_mut(&mut self) -> &mut Array {
        self.as_gcvalue_mut().as_array_mut()
    }

    pub fn try_as_array_mut(&mut self) -> Result<&mut Array> {
        if let Value::GcValue(gcvalue) = self {
            if let GcValueType::Array = gcvalue.tag() {
                return Ok(gcvalue.as_array_mut());
            }
        }
        Err("not array")
    }

    pub fn as_object(&self) -> &Object {
        self.as_gcvalue().as_object()
    }

    pub fn try_as_object(&self) -> Result<&Object> {
        if let Value::GcValue(gcvalue) = self {
            if let GcValueType::Object = gcvalue.tag() {
                return Ok(gcvalue.as_object());
            }
        }
        Err("not object")
    }

    pub fn as_object_mut(&mut self) -> &mut Object {
        self.as_gcvalue_mut().as_object_mut()
    }

    pub fn try_as_object_mut(&mut self) -> Result<&mut Object> {
        if let Value::GcValue(gcvalue) = self {
            if let GcValueType::Object = gcvalue.tag() {
                return Ok(gcvalue.as_object_mut());
            }
        }
        Err("not object")
    }

    pub fn get_primitive(&self) -> Value {
        if let Value::GcValue(gcvalue) = self {
            if let GcValueType::Object = gcvalue.tag() {
                return gcvalue.as_object().parent.get_primitive();
            }
        }
        *self
    }

    #[rustfmt::skip]
    pub fn primitive_method_call(
        ctx: &mut VMContext,
        method: &str,
        arguments: &[Value],
    ) -> Result<Value> {
        let first = arguments[0];
        Ok(match first {
            Value::Null if arguments.len() == 2 => {
                let second = arguments[1];
                match (method, second.typ()) {
                    ("==" | "eq", ValueType::Null) => ctx.create_bool(true),
                    ("!=" | "neq", ValueType::Null) => ctx.create_bool(false),
                    ("==" | "eq", _) => ctx.create_bool(false),
                    ("!=" | "neq", _) => ctx.create_bool(true),
                    _ => return Err("invalid primitive method call on null"),
                }
            }
            Value::Boolean(first) if arguments.len() == 2 => {
                let second = arguments[1];
                match (method, second.typ()) {
                    ("&" | "and", ValueType::Boolean) => ctx.create_bool(first && second.as_boolean()),
                    ("|" | "or", ValueType::Boolean) => ctx.create_bool(first || second.as_boolean()),

                    ("==" |  "eq", ValueType::Boolean) => ctx.create_bool(first == second.as_boolean()),
                    ("!=" | "neq", ValueType::Boolean) => ctx.create_bool(first != second.as_boolean()),
                    ("==" |  "eq", _) => ctx.create_bool(false),
                    ("!=" | "neq", _) => ctx.create_bool(true),
                    _ => return Err("invalid primitive method call on boolean"),
                }
            }
            Value::Integer(first) if arguments.len() == 2 => {
                let second = arguments[1];
                match (method, second.typ()) {
                    ("+" | "add", ValueType::Integer) => ctx.create_int(first + second.as_integer()),
                    ("-" | "sub", ValueType::Integer) => ctx.create_int(first - second.as_integer()),
                    ("/" | "div", ValueType::Integer) => ctx.create_int(first / second.as_integer()),
                    ("*" | "mul", ValueType::Integer) => ctx.create_int(first * second.as_integer()),
                    ("%" | "mod", ValueType::Integer) => ctx.create_int(first % second.as_integer()),

                    (">"  | "gt", ValueType::Integer) => ctx.create_bool(first >  second.as_integer()),
                    ("<"  | "lt", ValueType::Integer) => ctx.create_bool(first <  second.as_integer()),
                    (">=" | "ge", ValueType::Integer) => ctx.create_bool(first >= second.as_integer()),
                    ("<=" | "le", ValueType::Integer) => ctx.create_bool(first <= second.as_integer()),

                    ("==" |  "eq", ValueType::Integer) => ctx.create_bool(first == second.as_integer()),
                    ("!=" | "neq", ValueType::Integer) => ctx.create_bool(first != second.as_integer()),
                    ("==" |  "eq", _) => ctx.create_bool(false),
                    ("!=" | "neq", _) => ctx.create_bool(true),
                    _ => return Err("invalid primitive method call on integer"),
                }
            },
            Value::GcValue(mut first) => match first.tag() {
                GcValueType::Array => match method {
                    "get" if arguments.len() == 2 => {
                        first.as_array().get_index(arguments[1].try_as_integer()?)?
                    }
                    "set" if arguments.len() == 3 => {
                        first
                            .as_array_mut()
                            .set_index(arguments[1].try_as_integer()?, arguments[2])?;
                        Value::null()
                    }
                    _ => return Err("invalid method call on array"),
                },
                _ => return Err("invalid primitive method call"),
            },
            _ => return Err("invalid primitive method call"),
        })
    }
}

impl GcValue {
    pub fn new(ptr: *mut GcValueBase) -> Self {
        Self(ptr)
    }

    pub fn tag(&self) -> GcValueType {
        unsafe { ptr::read(self.0) }.tag
    }

    pub fn size(&self) -> usize {
        match self.tag() {
            GcValueType::Array => self.as_array().size(),
            GcValueType::Object => self.as_object().size(),
            GcValueType::Forwarder => unreachable!(),
        }
    }

    pub fn as_ptr(&self) -> *mut u8 {
        self.0 as *mut u8
    }

    pub fn get_forwarding_addr(&self) -> Option<GcValue> {
        match self.tag() {
            GcValueType::Forwarder => Some(self.get_forwarding_addr_raw()),
            _ => None,
        }
    }

    fn get_forwarding_addr_raw(&self) -> GcValue {
        unsafe { (*(self.0 as *const Forwarder)).new_addr }
    }

    pub fn as_array(&self) -> &Array {
        unsafe { &(*(self.0 as *const Array)) }
    }

    pub fn as_array_mut(&mut self) -> &mut Array {
        unsafe { &mut (*(self.0 as *mut Array)) }
    }

    pub fn as_object(&self) -> &Object {
        unsafe { &(*(self.0 as *const Object)) }
    }

    pub fn as_object_mut(&mut self) -> &mut Object {
        unsafe { &mut (*(self.0 as *mut Object)) }
    }
}

#[derive(Debug, Clone, Copy)]
pub enum FieldValue {
    Value(Value),
    Method(ConstantIndex),
}

pub struct Field {
    pub name: ConstantIndex,
    pub value: FieldValue,
}

#[derive(Debug)]
#[repr(C)]
pub struct GcValueBase {
    tag: GcValueType,
}

#[derive(Debug)]
#[repr(C)]
pub struct Array {
    tag: GcValueType,
    len: usize,
    values: [Value; 0],
}

impl Array {
    pub fn new(len: usize) -> Self {
        Self {
            tag: GcValueType::Array,
            len,
            values: [],
        }
    }

    pub fn size(&self) -> usize {
        mem::size_of::<Self>() + mem::size_of::<Value>() * self.len
    }

    pub fn as_slice(&self) -> &[Value] {
        unsafe { slice::from_raw_parts(self.values.as_ptr(), self.len) }
    }

    pub fn as_slice_mut(&mut self) -> &mut [Value] {
        unsafe { slice::from_raw_parts_mut(self.values.as_mut_ptr(), self.len) }
    }

    pub fn get_index(&self, index: i32) -> Result<Value> {
        if let Ok(index) = usize::try_from(index) {
            if index < self.len {
                return Ok(self[index]);
            }
        }
        Err("invalid set index into array")
    }

    pub fn set_index(&mut self, index: i32, value: Value) -> Result<()> {
        if let Ok(index) = usize::try_from(index) {
            if index < self.len {
                self[index] = value;
                return Ok(());
            }
        }
        Err("invalid set index into self")
    }
}

impl Index<usize> for Array {
    type Output = Value;
    fn index(&self, index: usize) -> &Self::Output {
        unsafe { self.values.get_unchecked(index) }
    }
}

impl IndexMut<usize> for Array {
    fn index_mut(&mut self, index: usize) -> &mut Self::Output {
        unsafe { self.values.get_unchecked_mut(index) }
    }
}

impl<'a> IntoIterator for &'a Array {
    type Item = &'a Value;
    type IntoIter = slice::Iter<'a, Value>;
    fn into_iter(self) -> Self::IntoIter {
        self.as_slice().iter()
    }
}

impl<'a> IntoIterator for &'a mut Array {
    type Item = &'a mut Value;
    type IntoIter = slice::IterMut<'a, Value>;
    fn into_iter(self) -> Self::IntoIter {
        self.as_slice_mut().iter_mut()
    }
}

#[repr(C)]
pub struct Object {
    tag: GcValueType,
    pub parent: Value,
    field_cnt: usize,
    fields: [Field; 0],
}

impl Object {
    pub fn new(field_count: usize) -> Self {
        Self {
            tag: GcValueType::Object,
            parent: Value::null(),
            field_cnt: field_count,
            fields: [],
        }
    }

    pub fn set_parent(&mut self, parent: Value) {
        self.parent = parent;
    }

    pub fn size(&self) -> usize {
        mem::size_of::<Self>() + mem::size_of::<Field>() * self.field_cnt
    }

    pub fn as_slice(&self) -> &[Field] {
        unsafe { slice::from_raw_parts(self.fields.as_ptr(), self.field_cnt) }
    }

    pub fn as_slice_mut(&mut self) -> &mut [Field] {
        unsafe { slice::from_raw_parts_mut(self.fields.as_mut_ptr(), self.field_cnt) }
    }

    pub fn get_field_raw(&self, name: ConstantIndex) -> Result<(Value, FieldValue)> {
        for field in self {
            if name == field.name {
                return Ok((
                    Value::GcValue(GcValue::new(self as *const Object as *mut GcValueBase)),
                    field.value,
                ));
            }
        }
        match self.parent.try_as_object() {
            Ok(parent) => parent.get_field_raw(name),
            Err(_) => Err("no field with such name"),
        }
    }

    pub fn get_field(&self, name: ConstantIndex) -> Result<Value> {
        let (_, field_value) = self.get_field_raw(name)?;
        match field_value {
            FieldValue::Value(value) => Ok(value),
            FieldValue::Method(_) => Err("field is a method"),
        }
    }

    pub fn get_method(&self, name: ConstantIndex) -> Result<(Value, ConstantIndex)> {
        let (obj, field_value) = self.get_field_raw(name)?;
        match field_value {
            FieldValue::Method(index) => Ok((obj, index)),
            FieldValue::Value(_) => Err("field is not a method"),
        }
    }

    pub fn set_index(&mut self, index: usize, name: ConstantIndex, value: FieldValue) {
        self[index] = Field { name, value };
    }

    pub fn set_field(&mut self, name: ConstantIndex, value: Value) -> Result<()> {
        for field in self.into_iter() {
            if name == field.name {
                match field.value {
                    FieldValue::Value(_) => {
                        field.value = FieldValue::Value(value);
                        return Ok(());
                    }
                    FieldValue::Method(_) => return Err("cannot assign to method field"),
                }
            }
        }
        match self.parent.try_as_object_mut() {
            Ok(parent) => parent.set_field(name, value),
            Err(_) => Err("no field with such name to set"),
        }
    }
}

impl Index<usize> for Object {
    type Output = Field;
    fn index(&self, index: usize) -> &Self::Output {
        unsafe { self.fields.get_unchecked(index) }
    }
}

impl IndexMut<usize> for Object {
    fn index_mut(&mut self, index: usize) -> &mut Self::Output {
        unsafe { self.fields.get_unchecked_mut(index) }
    }
}

impl<'a> IntoIterator for &'a Object {
    type Item = &'a Field;
    type IntoIter = slice::Iter<'a, Field>;
    fn into_iter(self) -> Self::IntoIter {
        self.as_slice().iter()
    }
}

impl<'a> IntoIterator for &'a mut Object {
    type Item = &'a mut Field;
    type IntoIter = slice::IterMut<'a, Field>;
    fn into_iter(self) -> Self::IntoIter {
        self.as_slice_mut().iter_mut()
    }
}

#[repr(C)]
pub struct Forwarder {
    tag: GcValueType,
    new_addr: GcValue,
}

impl Forwarder {
    pub fn new(new_addr: GcValue) -> Self {
        Self {
            tag: GcValueType::Forwarder,
            new_addr,
        }
    }
}

pub struct DisplayValue<'a>(pub Value, pub &'a Constants);

impl<'a> Display for DisplayValue<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let DisplayValue(value, constants) = *self;
        match value {
            Value::Null => write!(f, "null"),
            Value::Boolean(b) => write!(f, "{}", b),
            Value::Integer(i) => write!(f, "{}", i),
            Value::GcValue(value) => match value.tag() {
                GcValueType::Array => {
                    let mut values = Vec::new();
                    for &element in value.as_array() {
                        values.push(DisplayValue(element, constants).to_string());
                    }
                    write!(f, "[{}]", values.join(", "))
                }
                GcValueType::Object => {
                    let object = value.as_object();
                    let mut keys = Vec::new();
                    for field in value.as_object() {
                        if matches!(field.value, FieldValue::Value(_)) {
                            keys.push((
                                constants.get(field.name).unwrap().as_string().unwrap(),
                                field.name,
                            ));
                        }
                    }
                    keys.sort();

                    let mut members = Vec::new();
                    if !object.parent.is_null() {
                        members.push((
                            "..".to_string(),
                            DisplayValue(object.parent, constants).to_string(),
                        ));
                    }
                    for (key, name) in keys {
                        let value = object.get_field(name).unwrap();
                        members.push((key.to_string(), DisplayValue(value, constants).to_string()));
                    }
                    write!(
                        f,
                        "object({})",
                        members
                            .into_iter()
                            .map(|(name, value)| [name, value].join("="))
                            .collect::<Vec<_>>()
                            .join(", ")
                    )
                }
                GcValueType::Forwarder => write!(f, "<forwarding ptr>"),
            },
        }
    }
}
