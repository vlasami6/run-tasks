#[derive(Debug, PartialEq)]
pub enum TokenKind {
    Number(i32),
    Identifier(String),
    String(String),
    Op(&'static str),

    Semicolon, // ;
    LParen,    // (
    RParen,    // )
    Equal,     // =
    LArrow,    // <-
    RArrow,    // ->
    Dot,       // .
    LBracket,  // [
    RBracket,  // ]
    Comma,     // ,

    Begin,
    End,
    If,
    Then,
    Else,
    Let,
    Null,
    Print,
    Object,
    Extends,
    While,
    Do,
    Function,
    Array,
    True,
    False,

    Eof,

    Error(&'static str),
}

impl TokenKind {
    fn identifier_or_keyword(id: &str) -> TokenKind {
        use TokenKind::*;
        match id {
            "begin" => Begin,
            "end" => End,
            "if" => If,
            "then" => Then,
            "else" => Else,
            "let" => Let,
            "null" => Null,
            "print" => Print,
            "object" => Object,
            "extends" => Extends,
            "while" => While,
            "do" => Do,
            "function" => Function,
            "array" => Array,
            "true" => True,
            "false" => False,
            _ => Identifier(id.to_string()),
        }
    }
}

#[derive(Debug, PartialEq)]
pub struct Location {
    pub line: usize,
    pub col: usize,
}

#[derive(Debug, PartialEq)]
pub struct Token {
    pub kind: TokenKind,
    pub loc: Location,
}

#[derive(Debug)]
enum State {
    Start,
    Identifier,
    Integer,
    String,
    StringEscape,
    Slash,
    LineComment,
    BlockComment,
    BlockCommentStar,
    Minus,
    Equal,
    Greater,
    Less,
    Exclam,
}

pub struct Lexer<'a> {
    pub buffer: &'a str,
    position: usize,
    start: usize,
    line: usize,
    line_start: usize,
}

impl<'a> Lexer<'a> {
    pub fn new(input: &'a str) -> Self {
        Self {
            buffer: input,
            position: 0,
            start: 0,
            line: 0,
            line_start: 0,
        }
    }

    pub fn next(&mut self) -> Token {
        let kind = self.next_kind();
        Token {
            kind,
            loc: Location {
                line: self.line,
                col: self.start - self.line_start,
            },
        }
        //println!("lexer next: {:?}", tok);
        //tok
    }

    fn current_slice(&self) -> &'a str {
        &self.buffer[self.start..self.position]
    }

    fn next_kind(&mut self) -> TokenKind {
        self.start = self.position;
        let mut string = std::string::String::new();
        let mut state: State = State::Start;
        let iter = self.buffer[self.position..].bytes();
        use TokenKind::*;
        for c in iter {
            self.position += 1;
            //println!("{:?} {} \"{}\"", state, c as char, self.current_slice());
            match state {
                State::Start => match c {
                    b' ' | b'\t' => self.start += 1,
                    b'\n' => {
                        self.line_start = self.position;
                        self.line += 1;
                        self.start += 1;
                    }
                    b'A'..=b'Z' | b'a'..=b'z' | b'_' => state = State::Identifier,
                    b'0'..=b'9' => state = State::Integer,
                    b'"' => state = State::String,
                    b'/' => state = State::Slash,
                    b'-' => state = State::Minus,
                    b'=' => state = State::Equal,
                    b'>' => state = State::Greater,
                    b'<' => state = State::Less,
                    b'!' => state = State::Exclam,
                    b';' => return Semicolon,
                    b'|' => return Op("|"),
                    b'&' => return Op("&"),
                    b'+' => return Op("+"),
                    b'*' => return Op("*"),
                    b'%' => return Op("%"),
                    b'(' => return LParen,
                    b')' => return RParen,
                    b'.' => return Dot,
                    b'[' => return LBracket,
                    b']' => return RBracket,
                    b',' => return Comma,
                    _ => return Error("unexpected character"),
                },
                State::Identifier => match c {
                    b'A'..=b'Z' | b'a'..=b'z' | b'_' | b'0'..=b'9' => {}
                    _ => {
                        self.position -= 1;
                        return TokenKind::identifier_or_keyword(self.current_slice());
                    }
                },
                State::Integer => match c {
                    b'0'..=b'9' => {}
                    _ => {
                        self.position -= 1;
                        return Number(self.current_slice().parse().unwrap());
                    }
                },
                State::String => match c {
                    b'"' => return TokenKind::String(string),
                    b'\\' => state = State::StringEscape,
                    _ => string.push(c as char),
                },
                State::StringEscape => {
                    state = State::String;
                    match c {
                        b'n' => string.push('\n'),
                        b't' => string.push('\t'),
                        b'r' => string.push('\r'),
                        b'~' => string.push('~'),
                        b'"' => string.push('"'),
                        b'\\' => string.push('\\'),
                        _ => return Error("invalid string escape sequence"),
                    }
                }
                State::Slash => match c {
                    b'/' => {
                        state = State::LineComment;
                        self.start += 2;
                    }
                    b'*' => {
                        state = State::BlockComment;
                        self.start += 2;
                    }
                    _ => {
                        self.position -= 1;
                        return Op("/");
                    }
                },
                State::LineComment => {
                    #[allow(clippy::single_match)]
                    match c {
                        b'\n' => {
                            state = State::Start;
                            self.line_start = self.start;
                            self.line += 1;
                        }
                        _ => {}
                    };
                    self.start += 1;
                }
                State::BlockComment => {
                    match c {
                        b'*' => state = State::BlockCommentStar,
                        b'\n' => {
                            self.line_start = self.start;
                            self.line += 1;
                        }
                        _ => {}
                    };
                    self.start += 1;
                }
                State::BlockCommentStar => {
                    match c {
                        b'*' => {}
                        b'/' => {
                            state = State::Start;
                        }
                        b'\n' => {
                            self.line_start = self.start;
                            self.line += 1;
                        }
                        _ => {
                            state = State::BlockComment;
                        }
                    };
                    self.start += 1;
                }
                State::Minus => match c {
                    b'>' => return RArrow,
                    b'0'..=b'9' => state = State::Integer,
                    _ => {
                        self.position -= 1;
                        return Op("-");
                    }
                },
                State::Equal => match c {
                    b'=' => return Op("=="),
                    _ => {
                        self.position -= 1;
                        return Equal;
                    }
                },
                State::Greater => match c {
                    b'=' => return Op(">="),
                    _ => {
                        self.position -= 1;
                        return Op(">");
                    }
                },
                State::Less => match c {
                    b'=' => return Op("<="),
                    b'-' => return LArrow,
                    _ => {
                        self.position -= 1;
                        return Op("<");
                    }
                },
                State::Exclam => match c {
                    b'=' => return Op("!="),
                    _ => return Error("expected '!='"),
                },
            }
        }
        match state {
            State::Start | State::LineComment => Eof,
            State::Identifier => TokenKind::identifier_or_keyword(self.current_slice()),
            State::Integer => Number(self.current_slice().parse().unwrap()),
            State::String | State::StringEscape => Error("unterminated string"),
            State::Slash => Op("/"),
            State::BlockComment | State::BlockCommentStar => Error("untermintated block comment"),
            State::Minus => Op("-"),
            State::Equal => Op("="),
            State::Greater => Op(">"),
            State::Less => Op("<"),
            State::Exclam => Error("expected '!='"),
        }
    }
}
