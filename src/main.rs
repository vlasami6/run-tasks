mod ast;
mod bytecode;
mod compiler;
mod lexer;
mod parser;
mod runtime;
mod value;
mod vm;

use std::{error::Error, fs, io};

use bytecode::{BinaryRead, BinaryWrite, Program};
use compiler::Compiler;
use parser::Parser;
use vm::VM;

fn compile_file(filename: String) -> Result<Program, Box<dyn Error>> {
    let input = fs::read_to_string(filename.clone())?;
    let mut parser = Parser::new(&input);
    let root_nodes = parser.parse()?;
    Ok(Compiler::new().compile_program(&filename, &root_nodes)?)
}

fn read_bytecode(filename: String) -> Result<Program, Box<dyn Error>> {
    let mut file_reader = fs::File::open(filename)?;
    Ok(Program::read(&mut file_reader)?)
}

fn run_file<F>(mut args: std::env::Args, mut get_program: F) -> Result<(), Box<dyn Error>>
where
    F: FnMut(String) -> Result<Program, Box<dyn Error>>,
{
    let mut heap_size = 100;
    let mut heap_log: Option<Box<dyn std::io::Write>> = None;
    let mut filename = None;
    while let Some(arg) = args.next() {
        match &arg[..] {
            "--heap-size" => {
                heap_size = args
                    .next()
                    .expect("expected heap size argument")
                    .parse()
                    .expect("heap size should be a number")
            }
            "--heap-log" => {
                let log_file = args.next().expect("expected log file name");
                let log_file = fs::File::create(log_file)?;
                heap_log = Some(Box::new(std::io::BufWriter::new(log_file)));
            }
            _ => {
                filename = Some(arg);
            }
        }
    }
    let filename = filename.expect("expected file name to run");
    let program = get_program(filename)?;
    //eprintln!("program: {:?}", program);
    VM::new(program, heap_size, heap_log)?.run()?;
    Ok(())
}

fn main() -> Result<(), Box<dyn Error>> {
    let mut args = std::env::args();
    args.next(); // program name
    let command = args.next().expect("no command given");
    match &command[..] {
        "parse" => {
            let input_file = args.next().expect("expected input file name");
            args.next().expect("expected '-o'");
            let output_file = args.next().expect("expected output file name");
            io::copy(
                &mut fs::File::open(input_file).expect("failed to open input file"),
                &mut fs::File::create(output_file).expect("failed to open output file"),
            )
            .expect("failed to write output");
        }
        "run" => {
            run_file(args, compile_file)?;
        }
        "compile" => {
            let filename = args.next().expect("expected file name");
            let program = compile_file(filename)?;
            //eprintln!("program: {:?}", program);
            program.write(&mut std::io::stdout())?;
        }
        "execute" => {
            run_file(args, read_bytecode)?;
        }
        _ => panic!("Unknown command {}", command),
    }
    Ok(())
}
