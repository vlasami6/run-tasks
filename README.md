# NI-RUN tasks

```
git clone https://gitlab.fit.cvut.cz/vlasami6/run-tasks
git clone https://github.com/kondziu/FML
git clone https://github.com/kondziu/FMLtest
(cd run-tasks; ./build)
(cd FML; ./build)
export FML=$(readlink -f run-tasks/target/release/fml)
export FML_REF_BC_INT=$(readlink -f run-tasks/target/release/fml)
(cd FMLtest; ./suite fml $(./suite show fml) && ./suite bc $(./suite show bc) && ./suite comp $(./suite show comp))
```

I.e. I have my own `fml` executable as parser / compiler / interpreter and the
reference is only used as reference bytecode interpreter.

## 0. Introduction to FML

Git tag `task0`.

Programs contained in examples subdirectory.


## 1. FML AST Interpreter

Git tag `task1`.

All tests pass, though I have done some things differently than is expected, so
the solution is not "correct", and even the things are I aimed for are not 100
% complete.

My changes:

 - custom DFA lexer,
 - custom recursive descent (+ Pratt expression parser)
 - functions are first class
 - function calls are just calls of the `call` method on the function primitive type
 - calling objects is likewise possible through the `call` method
 - the scoping is currently horribly wrong
 - I am aiming for full lexical scoping, though this is not yet done (I'll have see about global scope and closures)
 - I use Rust's standard `HashMap` for object members, so the printing order may be wrong (AFAIK)

## 2. FML BC Interpreter

Git tag `task2-3`.

## 3. FML BC Compiler

Git tag `task3-3`

Like in the previous task I didn't do anything fancy.

## 4. GC

Git tag `task4`

I did the two space copying GC (Cheney's algorithm). More details in the
benchmark report (`benchmark/report.html`).


Michal Vlasák, summer semester 2021/2022
