---
title: "FML implementation performance evaluation"
author: "Michal Vlasák"
date: "4/11/2021"
output: html_document
---






This report describes the results of a performance comparison between multiple implementations of the FML language using a suite of benchmark programs.

## System description

The benchmark was executed using the followins system specs.

| Parameter                      | Spec                  | Notes |
| :---------------               | :-------------        | :---- |
| Processor                      | Intel(R) Core(TM) i5-10400 CPU @ 2.90GHz                  |       |
| Memory                         | 16 GiB                  |       |
| OS & kernel                    | Arch Linux, kernel 5.17.5                  |       |

## Benchmarks

The experiment was run using three benchmark programs. Each benchmark was executed 10 times (iterations 1-10).

| Label                          | Description           | URL                                                                       |
| :---------------               | :-------------        | :---------                                                                |
| `langtons_ant`                 | Langton's ant         | https://github.com/kondziu/FMLBench/blob/main/benchmarks/langtons_ant.fml |
| `brainfuck`                    | Brainfuck interpreter | https://github.com/kondziu/FMLBench/blob/main/benchmarks/brainfuck.fml    |
| `sudoku`                       | Sudoku solver         | https://github.com/kondziu/FMLBench/blob/main/benchmarks/sudoku.fml       |

### Langton's ant

An ant moves on a world of square tiles. The ant has position on a specific tile and an orientation (up, down, left, right). The tiles are initiall all white, but each tile can be either black or white. The program runs for 201 steps. In each step the ant moves one tile according to the following rules.

* If the ant is on a while tile, it changes the tile's color to black, rotates clockwise and moves one tile forward.
* If the ant is on a black tile, it changes the tile's color to white, rotates counterclockwise and moves one tile forward.

The board and the ant are printed after each step. The world starts empty and expands whenever the ant moves to a new tile.

[[wiki]](https://en.wikipedia.org/wiki/Langton%27s_ant)

### Brainfuck interpreter

An implementation of the Brainfuck language executing a specific program.

The language contains 8 instructions `.,<>[]+-`, 7 of which are expressable in FML (FML cannot read from stdin, preventing it from implementing `,`). The interpreter has a memory of 512 integers and a memory pointer. The interpreter moves the pointer to the left and right (`<` and `>`), increments and decrements the values at the pointer (`+` and `-`), and prints out the value of a cell as ASCII (`.`). `[` and `]` imeplement loops: `[` checks if the value of the cell at the pointer is 0, and proceeds to a matching `]` if it is or executes the next statement if it isn't. `]` returns to the matching `[`.

In this implementation the memory consists of 512 32bit integers. The memory pointer is initially positioned in the middle.

The program executed by the benchmark prints out a poem in 429 instructions.

[[wiki]](https://en.wikipedia.org/wiki/Brainfuck)

### Sudoku solver

A brute force sudoku solver. It starts from the top-left corner of the board, adds a number from 1-9 (in order) in the first available empty spot and checks whether the board is still valid. It continues until the end of the board and backtracks to if the board becomes invalid at any point.

[[wiki]](https://en.wikipedia.org/wiki/Sudoku)

## Implementations

The experiment was run using the following implementations of the FML language. 

| Label                          | Description                       | URL                                    |
| :---------------               | :-------------                    | :---------                             |
| `./fml` | The reference FML implementation. | https://github.com/kondziu/FML         |
| `./ast` | AST interpreter with cheat representation and no GC      | https://gitlab.fit.cvut.cz/vlasami6/run-tasks/tree/ast_interpreter           |
| `./bc`  | Bytecode interpreter with cheat representation and no GC | https://gitlab.fit.cvut.cz/vlasami6/run-tasks/tree/bc_interpreter_without_gc |
| `./gc`  | Bytecode interpreter with proper representation and GC   | https://gitlab.fit.cvut.cz/vlasami6/run-tasks/tree/task4                     |

The `./fml` implementation is written in Rust. The stack consists of a bytecode
compiler and a bytecode interpreter. Execution times include parsing,
compilation, and execution. The implementation does not garbage collect, so it
does not incur garbage-collection-related overheads.

All my interpreters were written in Rust, with custom parser and AST.

I wanted to include `./ast` in the benchmarks, but it turns out it is bugged,
and because of some differences (like try of first class functions) I didn't
feel like fixing it, so I left it out.

The core of both the bytecode interpreters is pretty much the same (the
bytecode interpreter loop and the bytecode compiler)

In the case of `./bc` I used the data representation from the AST interpreter -
`Vec`s for arrays and `HashMap`s for objects. This is kind of cheating, and
wouldn't play nice with a GC either (because I would manage the "pointers" and
"metadata" of `Vec`s and `HashMap`s, while they own the memory).

In the case of `./gc` I completely changed the representation of all program
objects. Pointer tagging from previous incarnation is kept, but garbage
collected objects (arrays and objects) are now based on the C style "struct
inheritance" and all objects are stored in contiguous memory using the 2 space
copying GC technique (Cheney's algorithm). A third kind of garbage collected
object ("forwarding pointer") is introduced for GC purposes. For the sake of
the object representation, some compromises were made -- the object fields are
now stored as an array of `enum`s and can thus be both function's constant
indicies or values of fields. For simplicity field _names_ are only stored and
compared only as constant indicess, which would not work for all compiler
designs (but my compiler interns all constants, and it also work with the
bytecode produced by the reference compiler). Because global environment is now
not a special construct, but an ordinary object (with null parent), the
tradeoffs translate to here as well.

## Correctness 

The benchmarks were sanity checked after execution by comparing outputs.

![plot of chunk correctness](figure/correctness-1.png)

#### Observations

Since I left out the AST interpreter altogether, all tests pass.

## Execution time

![plot of chunk execution-time](figure/execution-time-1.png)

#### Observations

My garbage collecting interpreter seems to be the fastest. It is very probable that it is due to the different represenation of data, where I never compare strings as strings, but only as their indicdes in constant pool. On the contrary I do `O(n)` search for object fields, while hash map approaches could do `O(1)`. Though this probably didn't matter much, since `n` is very small here (although I should note that in my linear case `n` is fields + methods not just fields as is with my hashmap approach).

## Memory usage

(These can take a while to generate).

```
## Rows: 4 Columns: 3
## ── Column specification ────────────────────────────────────────────────────────────────────────────────
## Delimiter: ","
## chr (1): event
## dbl (2): timestamp, heap
## 
## ℹ Use `spec()` to retrieve the full column specification for this data.
## ℹ Specify the column types or set `show_col_types = FALSE` to quiet this message.
```

```
## Error in `add_theme()`:
## ! Can't add `scale_y_continuous(labels = function(x) ifelse(x >= 1024 * 1024 * ` to a theme object.
## • Can't add `    1014, paste0(as.integer(x/1024/1024/1024), "GB"), ifelse(x >= ` to a theme object.
## • Can't add `    1024 * 1024, paste0(as.integer(x/1024/1024), "MB"), ifelse(x >= ` to a theme object.
## • Can't add `    1024, paste0(as.integer(x/1024), "KB"), paste0(x, "B")))))` to a theme object.
```

#### Observations

Sorry I had no idea how to fix the R errors. According to the logs, my GC only did something in sudoku, where it worked pretty well, i.e. several occurrences of:

```
[...]
1651443715075962536,A,52428352
1651443715075962827,A,52428544
1651443715075963553,A,52428736
1651443715075967605,G,6976
1651443715075967809,A,7168
[...]
```
